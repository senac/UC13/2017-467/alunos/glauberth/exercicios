package br.com.senac.testes.teste5;

import br.com.senac.atividade5.Atividade5;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteAtividade5 {

    public TesteAtividade5() {
    }

    @Test
    public void DeveConverterParaPAR() {
        int valor = 10;

        boolean resultado = Atividade5.ConverterPARAPAR(valor);
        assertTrue(resultado);

    }

    @Test
    public void NaoDeveConverterParaPar() {
        int valor = 5;

        boolean resultado = Atividade5.ConverterPARAPAR(valor);
        assertFalse(resultado);
    }

    @Test
    public void DeveConverterParaImpar() {
        int valor = 7;

        boolean resultado = Atividade5.ConverterParaIMPAR(valor);
        assertTrue(resultado);

    }

    @Test
    public void NaoDeveConverterParaImpar() {

        int valor = 6;

        boolean resultado = Atividade5.ConverterParaIMPAR(valor);
        assertFalse(resultado);

    }

}

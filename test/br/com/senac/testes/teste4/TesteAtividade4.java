package br.com.senac.testes.teste4;

import br.com.senac.atividade4.Produto;
import br.com.senac.atividade4.Vendas;
import br.com.senac.atividade4.Vendedor;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteAtividade4 {

    public TesteAtividade4() {
    }

    /*
    @Test
    public void testeVenda0(){
        
        Vendedor vendedor = new Vendedor(1, "Glauberth");
        
        Produto bermuda = new Produto(1);            
        ItemDeVendas itemDeVendas = new ItemDeVendas(bermuda, 2, 25);
        
        Produto chaveiro = new Produto(2);
        ItemDeVendas itemDeVendas1 = new ItemDeVendas(chaveiro, 5 , 10);
        
        
        List<ItemDeVendas> itens = new ArrayList<>();
        itens.add(itemDeVendas);
        itens.add(itemDeVendas1);
        
        Vendas venda = new Vendas(vendedor, itens);
        assertEquals(5, venda.valorDaVenda(venda) , 0);        
        
    }
   
    
    @Test
    public void DeveDarErroDeComissao(){
        
        Vendedor vendedor = new Vendedor(2, "João");
        Produto mochila = new Produto(3);
        ItemDeVendas itemDeVendas = new ItemDeVendas(mochila, 2 ,10 );
        
        List<ItemDeVendas> itens = new ArrayList<>();
        itens.add(itemDeVendas);
        
        Vendas venda = new Vendas(vendedor, itens);
        assertNotEquals(2, venda.valorDaVenda(venda) , 0);
                
        
    }
     */
    @Test
    public void deveCalcularVenda() {

        Vendas venda = new Vendas(new Vendedor(1, "Jose"));

        Produto produto = new Produto(1, "Camisa");

        venda.adicionarItem(produto, 1, 10);

        double resultado = venda.getTotalVenda();

        assertEquals(resultado, 10, 0.05);  

    }

}

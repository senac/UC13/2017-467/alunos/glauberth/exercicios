
package br.com.senac.testes.teste4;

import br.com.senac.atividade4.Pagamento;
import br.com.senac.atividade4.Produto;
import br.com.senac.atividade4.Vendas;
import br.com.senac.atividade4.Vendedor;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class TesteFolhaDePagamento {
    
    public TesteFolhaDePagamento() {
    }
    
    
    @Test
    public void deveCalcularComissaoPraUmVendedor(){
        
        Vendedor vendedor = new Vendedor(1, "Glauberth");
        Produto produto = new Produto(1, "Camisa");
        
        Vendas venda = new Vendas(vendedor);
        venda.adicionarItem(produto, 1 , 10);
        
        Vendas venda1 = new Vendas(vendedor);
        venda1.adicionarItem(produto, 1, 10);
        
        List<Vendas> listaVenda = new ArrayList<>();
        listaVenda.add(venda);
        listaVenda.add(venda1);
        
        Pagamento folhaPagamento = new Pagamento();
        folhaPagamento.calcularComissao(listaVenda);
        
        double comissao = folhaPagamento.getComissaoDoVendedor(vendedor);
        
        assertEquals(1 , comissao , 0.01);
        
        
        
        
    }
    
    @Test
    public void deveCalcularComissaoParaDoisVendedores(){
        
        Vendedor glauberth = new Vendedor(1, "glauberth");
        Vendedor joao = new Vendedor(2, "João");
        
        Produto produto = new Produto(1, "Bermuda");
        
        Vendas venda = new Vendas(glauberth);
        venda.adicionarItem(produto, 10 , 10);
        
        Vendas venda1 = new Vendas(joao);
        venda1.adicionarItem(produto, 10, 20);
        
        List<Vendas> listaVendas = new ArrayList<>();
        listaVendas.add(venda);
        listaVendas.add(venda1);
        
        Pagamento folhaDePagamento = new Pagamento();
        folhaDePagamento.calcularComissao(listaVendas);
        
        double comissaoGlauberth = folhaDePagamento.getComissaoDoVendedor(glauberth);
        
        assertEquals(5.0, comissaoGlauberth , 0.01);
        
        
        
        
        
        
        
        
    }
    
}

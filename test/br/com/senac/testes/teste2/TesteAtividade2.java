
package br.com.senac.testes.teste2;

import br.com.senac.atividade2.Calculadora;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteAtividade2 {
    
    public TesteAtividade2() {
    }
    
    
    @Test
    public void calculaCustoFinalParaOConsumidor(){
        
        Calculadora calculadora = new Calculadora();
        
        double custo = 10000;
        double resultado = calculadora.calcula(custo);
        
        assertEquals(17300, resultado , 0.01);
        
    }
    
    
}


package br.com.senac.testes.teste9;

import br.com.senac.atividade9.Viagem;
import org.junit.Test;
import static org.junit.Assert.*;


public class TesteLitrosEDistancia {

    public TesteLitrosEDistancia() {
    }

    @Test
    public void deveCalcularDistancia() {
        Viagem v = new Viagem(5, 80);
        double resultado = v.calculaDistancia();
        assertEquals(400.0, resultado, 0);
        
    }

    @Test
    public void deveCalcularLitros() {

        Viagem v = new Viagem(5, 80);
        double resultado = v.calcularLitros();

        assertEquals(33.33, resultado, 0.3);

    }

}

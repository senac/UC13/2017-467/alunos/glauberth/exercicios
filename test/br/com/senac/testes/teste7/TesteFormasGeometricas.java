/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.testes.teste7;

import br.com.senac.atividade7.Circulo;
import br.com.senac.atividade7.Quadrado;
import br.com.senac.atividade7.Retangulo;
import br.com.senac.atividade7.Triangulo;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sala304b
 */
public class TesteFormasGeometricas {

    public TesteFormasGeometricas() {
    }

    @Test
    public void AreaDoTrianguloDeveSer25() {
        Triangulo triangulo = new Triangulo(10, 5);

        assertEquals(25, triangulo.Area(), 0);

    }

    @Test
    public void AreaDoCirculoDeveSer200e96() {
        Circulo circulo = new Circulo(8);

        assertEquals(200.96, circulo.Area(), 0);

    }

    @Test
    public void AreaDoQuadradoDeveSer100() {
        Quadrado quadrado = new Quadrado(10);
        assertEquals(100, quadrado.Area(), 0);

    }

    @Test
    public void AreaDoRetanguloDeveSer100() {
        Retangulo retangulo = new Retangulo(10, 10);
        assertEquals(100, retangulo.Area(), 0);
    }

}

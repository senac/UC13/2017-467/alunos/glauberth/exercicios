/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.testes.teste8;

import br.com.senac.atividade8.VendasLivros;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sala304b
 */
public class TesteVendaDeLivros {

    public TesteVendaDeLivros() {
    }

    @Test
    public void DeveCalcularImpostoPraRJ() {

        VendasLivros vendaRJ = new VendasLivros("Livraria RJ", 1000, "RJ");
        assertEquals(170, vendaRJ.VendasRJ(), 0);

    }
    
    @Test
    public void DeveCalcularImpostoPraSP(){
        
        VendasLivros vendaSP = new VendasLivros("Livraria SP", 1000, "SP" );
        assertEquals(180, vendaSP.VendasSP() , 0);
        
    }
        
    @Test
    public void DeveCalcularImpostoPraMA(){
        VendasLivros vendaMA = new VendasLivros("Livraria de MA", 1000 , "MA");
        assertEquals( 120 , vendaMA.VendasMA() , 0 );
        
    }
}

package br.com.senac.testes.teste1;

import br.com.senac.atividade1.Atividade1;
import br.com.senac.atividade1.Pessoa;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteAtividade1 {

    public TesteAtividade1() {
    }

    @Test
    public void amandaDeveSerAMaisNova() {

        List<Pessoa> lista = new ArrayList<>();
        Pessoa amanda = new Pessoa("Amanda", 19);
        Pessoa glauberth = new Pessoa("Glauberth", 21);
        Pessoa thais = new Pessoa("Thais", 24);

        lista.add(amanda);
        lista.add(glauberth);
        lista.add(thais);

        Pessoa maisNova = Atividade1.getPessoaMaisNova(lista);
        assertEquals(amanda, maisNova);

    }

    @Test
    public void thaisNaoDeveSerAMaisNova() {

        List<Pessoa> lista = new ArrayList<>();
        Pessoa amanda = new Pessoa("Amanda", 19);
        Pessoa glauberth = new Pessoa("Glauberth", 21);
        Pessoa thais = new Pessoa("Thais", 24);

        lista.add(amanda);
        lista.add(glauberth);
        lista.add(thais);

        Pessoa maisNova = Atividade1.getPessoaMaisNova(lista);
        assertNotEquals(thais, maisNova);
    }

    @Test
    public void thaisDeveSerAMaisVelha() {

        List<Pessoa> lista = new ArrayList<>();
        Pessoa amanda = new Pessoa("Amanda", 19);
        Pessoa glauberth = new Pessoa("Glauberth", 21);
        Pessoa thais = new Pessoa("Thais", 24);

        lista.add(amanda);
        lista.add(glauberth);
        lista.add(thais);
        
        Pessoa maisVelha = Atividade1.getPessoaMaisVelha(lista);
        assertEquals(thais, maisVelha);
        
    }
    
    @Test
    public void glauberthDeveSerOMaisVelho(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa amanda = new Pessoa("Amanda", 19);
        Pessoa glauberth = new Pessoa("Glauberth", 21);
        Pessoa thais = new Pessoa("Thais", 24);
        
        lista.add(amanda);
        lista.add(glauberth);
        lista.add(thais);
        
        Pessoa maisVelha = Atividade1.getPessoaMaisVelha(lista);
        assertNotEquals(glauberth, maisVelha);
    }
    

}

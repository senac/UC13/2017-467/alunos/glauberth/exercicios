package br.com.senac.testes.teste6;

import br.com.senac.atividade6.Aluno;
import br.com.senac.atividade6.VerificaNotaAluno;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteAluno {

    public TesteAluno() {
    }

    @Test
    public void alunoDeveSerAprovado() {

        Aluno aluno = new Aluno("Glauberth", 7);

        VerificaNotaAluno.aprovado(aluno);

        assertEquals(aluno, aluno);

    }

    @Test
    public void alunoDeveFicarNaMedia() {

        Aluno aluno1 = new Aluno("Joao", 5);
        boolean resultado = VerificaNotaAluno.media(aluno1);

        assertTrue(resultado);

    }

    @Test
    public void alunoDeveFicarDeRecuperacao() {
        Aluno a = new Aluno("Bruno", 2);
        boolean resultado = VerificaNotaAluno.recuperacao(a);

        assertTrue(resultado);

    }

}

package br.com.senac.testes.teste3;

import br.com.senac.atividade3.CalculadoraDeMoedas;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteAtividade3 {

    public TesteAtividade3() {
    }

    @Test
    public void ConverterParaDolarAMERICANO() {
        
        double real = 10;

        CalculadoraDeMoedas calculadora = new CalculadoraDeMoedas();
        double resultado = calculadora.CalculaDolarAMERICANO(real);
        
        assertEquals(40,resultado , 0.01);
        
        
    }
    
    @Test
    public void ConverterParaEURO(){
        double real = 10;
        
        CalculadoraDeMoedas calculadora = new CalculadoraDeMoedas();
        double resultado = calculadora.CalcularEURO(real);
        
        assertEquals(48, resultado , 0.01);
        
    }
    
    @Test
    public void ConverterParaDolarAustraliano(){
        
        double real = 10;
        CalculadoraDeMoedas calculadora = new CalculadoraDeMoedas();        
        double resultado = calculadora.CalculaDolarAustraliano(real);
        
        assertEquals(30, resultado , 0.01);
    }
    
    
}


package br.com.senac.testes.teste10;

import br.com.senac.atividade10.Alunos;
import br.com.senac.atividade10.NotaDosAlunos;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class TesteNotasAlunos {
    
    public TesteNotasAlunos() {
    }
    
    @Test
    public void DoisAlunosDeveSerAprovado(){
        List<Alunos> lista = new ArrayList<>();
        
        Alunos glauberth = new Alunos("Glauberth", 7 , 7);
        Alunos maria = new  Alunos("Maria", 10 , 10);
        
        Alunos joao = new  Alunos("Maria", 7 , 8);
        Alunos jorge = new  Alunos("Maria", 4 , 3);
        
        lista.add(glauberth);
        lista.add(maria);
        lista.add(joao);
        lista.add(jorge);
        
        double alunoAprovado = NotaDosAlunos.getNotasAlunos(lista);
        assertEquals(3 , alunoAprovado , 0);
        
       
        
        
    }
    
    
}

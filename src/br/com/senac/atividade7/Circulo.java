
package br.com.senac.atividade7;

public class Circulo extends FiguraGeometrica{

    private double raio;

    public Circulo(double raio) {
        this.raio = raio;
    }
    
    
    @Override
    public double Area() {
        return 3.14 * (this.raio * this.raio);
    }
  
    
}

package br.com.senac.atividade7;

public class Quadrado extends FiguraGeometrica {

    private double ladoA;

    public Quadrado(double ladoA) {
        this.ladoA = ladoA;
        
    }

    @Override
    public double Area() {

        return this.ladoA * this.ladoA;
    }

}

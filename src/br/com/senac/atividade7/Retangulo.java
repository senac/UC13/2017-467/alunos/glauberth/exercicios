package br.com.senac.atividade7;

public class Retangulo extends FiguraGeometrica {
    
    
    private double base;
    private double altura;

    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

            
    @Override
    public double Area() {
        return this.base * this.altura;
    }

}

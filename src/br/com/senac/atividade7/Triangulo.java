package br.com.senac.atividade7;

public class Triangulo extends FiguraGeometrica {

     private double base;
     private double altura;

    public Triangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    } 
    
    @Override
    public double Area() {
        
        return (this.altura * this.base) / 2;

    }
    
}

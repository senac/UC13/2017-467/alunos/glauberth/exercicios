package br.com.senac.atividade6;

public class VerificaNotaAluno {

    public final String aprovado = "Aprovado";
    public final String recuperacao = "Recuperação";
    public final String reprovado = "Reprovado";

    public static boolean aprovado(Aluno aluno) {

        return aluno.getNota() >= 7;
    }

    public static boolean media(Aluno aluno) {

        if (aluno.getNota() >= 4 && aluno.getNota() <= 6) {

            return true;

        }

        return false;
    }

    public static boolean recuperacao(Aluno aluno) {

        if (aluno.getNota() < 4) {

            return true;

        }
        return false;
    }

    public String resultadoAprovacao(Aluno aluno) {
        
        String resultadoFinal = null;

        if (aprovado(aluno)) {
            resultadoFinal = this.aprovado;
        } else if (recuperacao(aluno)) {
            resultadoFinal = this.reprovado;
        } else {
            resultadoFinal = this.recuperacao;
        }

        return resultadoFinal;

    }

}

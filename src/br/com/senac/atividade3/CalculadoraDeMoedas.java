package br.com.senac.atividade3;

import java.util.Scanner;

public class CalculadoraDeMoedas {
    
    
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("         CONVERSOR DE MOEDAS     $");
        System.out.println("Digite o valor do Real: ");
        double real = scanner.nextDouble();
        
        System.out.println("RESULTADO DA CONVERSÃO!!!!");
        System.out.println("---------------------------");
        
        double dolar = CalculaDolarAMERICANO(real);
        System.out.println("VALOR EM DOLAR AMERICANO: " + dolar);
        
        
        double euro = CalcularEURO(real);
        System.out.println("VALOR EM EURO: " + euro);
        
        double australiano = CalculaDolarAustraliano(real);
        System.out.println("VALOR EM DOLAR AUSTRALIANO: " + australiano);
       
    }
    

    public static double CalculaDolarAMERICANO(double real) {

        double dollarAmericano = real * 4;

        return dollarAmericano;

    }

    public static double CalcularEURO(double real) {

        double euro = real * 4.8;

        return euro;
    }
    
    public static double CalculaDolarAustraliano(double real){
        
        double dolarAustraliano = real * 3;
        
        return dolarAustraliano;
        
    }

}

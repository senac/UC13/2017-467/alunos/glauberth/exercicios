package br.com.senac.atividade5;

import java.util.Scanner;

public class Atividade5 {

    public static void main(String[] args) {

        int valor;
        System.out.println("Digite um valor: ");
        Scanner scanner = new Scanner(System.in);
        valor = scanner.nextInt();

        if (valor % 2 == 0 && valor > 0) {
            ConverterPARAPAR(valor);
            System.out.println("Este valor é par!");
            System.out.println("Este Valor é Positivo");

        } else if (valor % 2 == 1) {
            ConverterParaIMPAR(valor);
            System.out.println("Este valor é impar!");
            System.out.println("Este valor é Positivo");
        }else{
            System.out.println("Este valor é negativo");
        }

    }

    public static boolean ConverterParaIMPAR(int valor) {

        if (valor % 2 == 1 && valor > 0) {

            return true;

        }

        return false;
    }

    public static boolean ConverterPARAPAR(int valor) {

        if (valor % 2 == 0 && valor > 0) {

            return true;

        }

        return false;

    }

}

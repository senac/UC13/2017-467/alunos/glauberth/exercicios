
package br.com.senac.atividade8;


public class VendasLivros {
    
    private String cliente;
    private double valor;
    private String estado;

    public VendasLivros(String cliente, double valor, String estado) {
        this.cliente = cliente;
        this.valor = valor;
        this.estado = estado;
    }     
    
    public double VendasRJ(){        
        
        double imposto = (valor * 0.17);
        
                
        return imposto;
    }
    
    public double VendasSP(){
       
        double impostos = (valor * 0.18);
        
        return impostos;
    }
    
    public double VendasMA(){
        
        return valor * 0.12;
        
    }
    

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    } 
    
    
    
    }

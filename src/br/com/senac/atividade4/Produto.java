
package br.com.senac.atividade4;


public class Produto {
    
    private int codigo;
    private String nome;

    public Produto() {
    }

    public Produto(int codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    
}

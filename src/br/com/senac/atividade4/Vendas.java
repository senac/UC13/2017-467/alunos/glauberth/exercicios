package br.com.senac.atividade4;

import java.util.ArrayList;
import java.util.List;

public class Vendas {

    private Vendedor vendedor;
    private List<ItemDeVendas> itensVendas;

    public Vendas() {
        this.itensVendas = new ArrayList<>();
    }

    public Vendas(Vendedor vendedor) {
        this();
        this.vendedor = vendedor;
    }
    
    
    public Vendas(Vendedor vendedor, List<ItemDeVendas> itens) {
        this.itensVendas = itens;
        this.vendedor = vendedor;
    }

    public void adicionarItem(Produto produto, int quantidade, double valor) {

        this.itensVendas.add(new ItemDeVendas(produto, quantidade, valor));

    }

    public double valorDaVenda(Vendas venda) {

        double valorDaVenda = 0;

        for (ItemDeVendas itensVenda : itensVendas) {

            valorDaVenda += itensVenda.getTotal();

        }

        return valorDaVenda;

    }

    public List<ItemDeVendas> getItensVendas() {
        return itensVendas;
    }

    public void setItensVendas(List<ItemDeVendas> itensVendas) {
        this.itensVendas = itensVendas;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
    
    public double getTotalVenda(){
        double total = 0;
        
        for(ItemDeVendas item : this.itensVendas){
            total += item.getTotal();
        }
        
        return total;
        
    }
    
    
}

package br.com.senac.atividade4;

import java.util.HashMap;
import java.util.List;

public class Pagamento {

    private HashMap<Integer, Double> comissoes = new HashMap<>();

    public Pagamento() {
    }

    private final double valorComissao = 0.05;

    public void calcularComissao(List<Vendas> lista) {

        for (Vendas v : lista) {

            int codigo = v.getVendedor().getCodigo();

            if (!comissoes.containsKey(codigo)) {
                comissoes.put(codigo, v.getTotalVenda() * valorComissao);
            }else{
                double c = comissoes.get(codigo).doubleValue();
                c += (v.getTotalVenda() * valorComissao);
                comissoes.put(codigo, c);
            }

        }

    }
    
    public double getComissaoDoVendedor(Vendedor vendedor){
        return this.comissoes.get(vendedor.getCodigo()).doubleValue();
    }
    
}   

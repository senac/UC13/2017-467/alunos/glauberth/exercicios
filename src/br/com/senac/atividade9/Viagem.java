package br.com.senac.atividade9;

public class Viagem {

    double consumo = 12;
    double tempo;
    double velocidade;

    double distancia;
    double litros;

    public Viagem(double tempo, double velocidade) {
        this.tempo = tempo;
        this.velocidade = velocidade;
    }

    public double calculaDistancia() {

        return this.distancia = this.tempo * this.velocidade;
    }

    public double calcularLitros() {

        return this.litros = calculaDistancia() / consumo;

    }

}

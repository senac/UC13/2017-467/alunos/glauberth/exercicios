
package br.com.senac.atividade2;

import java.util.Scanner;


public class Calculadora {
    
    private final double distribuidor = 0.28;
    private final double imposto = 0.45;
    
    
    public static void main(String[] args) {
        
        double custo;
        
        Scanner scaner = new Scanner(System.in);
        System.out.println("Digite o valor do Veiculo: ");
        custo = scaner.nextDouble();
        
        double resultado = calcula(custo);
        System.out.println("Preço final com Impostos : " + resultado + " $");
        
    }
    
    
    
    
   public static double calcula( double custo){
       
       double imposto = custo * 0.28;
       double distribuidor = custo * 0.45;
       double precoFinal = custo + imposto + distribuidor;
       
       
       return precoFinal ;
       
   } 
    
    
}

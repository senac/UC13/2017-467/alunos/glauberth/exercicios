package br.com.senac.atividade1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Atividade1 {

    public static void main(String[] args) {

        List<Pessoa> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        
        Pessoa p1 = new Pessoa();
        System.out.println("Nome:");
        p1.setNome(scanner.next());
        System.out.println("Idade:");
        p1.setIdade(scanner.nextInt());

        Pessoa p2 = new Pessoa();
        System.out.println("Nome:");
        p2.setNome(scanner.next());
        System.out.println("Idade:");
        p2.setIdade(scanner.nextInt());

        Pessoa p3 = new Pessoa();
        System.out.println("Nome:");
        p3.setNome(scanner.next());
        System.out.println("Idade:");
        p3.setIdade(scanner.nextInt());

        lista.add(p1);
        lista.add(p2);
        lista.add(p3);

        Pessoa maisVelha = getPessoaMaisVelha(lista);
        Pessoa maisNova = getPessoaMaisNova(lista);
        System.out.println("Pessoa Mais Nova: " + maisNova.getNome());
        System.out.println("Pessoa Mais Velha: " + maisVelha.getNome());

    }

    public static Pessoa getPessoaMaisNova(List<Pessoa> lista) {

        Pessoa maisNova = lista.get(0);

        for (Pessoa p : lista) {
            if (p.getIdade() < maisNova.getIdade()) {
                maisNova = p;

            }
        }
        return maisNova;
    }

    public static Pessoa getPessoaMaisVelha(List<Pessoa> lista) {

        Pessoa maisVelha = lista.get(2);

        for (Pessoa p : lista) {
            if (p.getIdade() > maisVelha.getIdade()) {
                maisVelha = p;

            }
        }

        return maisVelha;

    }
}

package br.com.senac.atividade10;

import java.util.ArrayList;
import java.util.List;

public class NotaDosAlunos {

    public final String aprovado = "Aluno Aprovado";
    public final String reprovado = "Não há Aluno com nota acima de 5";

    public static void main(String[] args) {

        

    }

    public static Alunos getAlunoReprovado(List<Alunos> listaAlunos) {

        Alunos alunoRerovado = listaAlunos.get(0);

        for (Alunos listaAluno : listaAlunos) {

            if (listaAluno.getNota() < alunoRerovado.getNota()) {
                alunoRerovado = listaAluno;
            }

        }
        return alunoRerovado;

    }

    public static double getNotasAlunos(List<Alunos> lista) {
       
        int contador = 0;

        for (Alunos alunos : lista) {
            double notaFinal = (alunos.getNota() + alunos.getNota2()) / 2;
            
            if (notaFinal >= 7) {
                contador = contador + 1;

            }

        }
        return contador;

    }

}

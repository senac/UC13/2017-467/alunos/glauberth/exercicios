
package br.com.senac.atividade10;


public class Alunos {
    
    private String nome;
    private int nota;
    private int nota2;

    public Alunos(String nome, int nota, int nota2) {
        this.nome = nome;
        this.nota = nota;
        this.nota2 = nota2;
    }

    
    public int getNota2() {
        return nota2;
    }

    public void setNota2(int nota2) {
        this.nota2 = nota2;
    }
    
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
    
    
    
    
    
    
}
